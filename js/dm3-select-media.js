/**
 * Dm3MediaFrame
 * @version 2.0
 */
if (typeof dm3SelectMedia === 'undefined') {
	(function($) {
		if (typeof wp.media === 'undefined') {
			return;
		}

		var theFrame = null;

		/**
		 * Add media frame.
		 */
		dm3_media_frame = function(params) {
			if (theFrame === null) {
				theFrame = wp.media.frames.dm3MediaFrame = wp.media({
					frame: 'post',
					state: 'embed',
					multiple: params.multiple
				});
			}

			theFrame.off('insert');
			theFrame.off('embed');

			if (typeof params.callback !== 'undefined') {
				theFrame.on('insert', function() {
					params.callback.call(null, theFrame.state());
				});

				theFrame.state('embed').on( 'select', function() {
					params.callback.call(null, theFrame.state());
				});
			}

			theFrame.open();
		};

		dm3_media_upload_button_click = function() {
			var button = $(this);
			var multiple = button.data('multiple');
			var size = button.data('size');

			if (typeof multiple === 'undefined') {
				multiple = false;
			}

			dm3_media_frame({
				multiple: multiple,
				insertLabel: button.data('insertlabel'),
				callback: function(state) {
					var item, url;
					var size = $('.attachment-display-settings select.size').val();

					if (state.id == 'embed') {
						item = state.props.toJSON();
						url = item.url;
					} else {
						item = state.get('selection').first();

						if (!item) {
							// Nothing selected.
							return;
						}

						if (size && item.attributes.sizes && typeof item.attributes.sizes[size] !== 'undefined') {
							url = item.attributes.sizes[size].url;
						} else {
							url = item.attributes.url;
						}
					}

					button.prev('input').val(url);
				}
			});
		};

		$(document).ready(function() {
			/**
			 * Media Uploader.
			 */
			$('body').on('click', '.upload-image-button', dm3_media_upload_button_click);
		});
	}(jQuery));
}
