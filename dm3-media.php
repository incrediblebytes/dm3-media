<?php
/**
 * Plugin Name: Dm3Media
 * Author: incrediblebytes
 * Author URI: http://incrediblebytes.com
 * Description: Adds ability to add various media (images, video, audio) to pages and posts.
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Version: 1.4.0
 * 
 * @package Dm3Media
 */

/*
Copyright (C) 2015 http://incrediblebytes.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'DM3MEDIA_URL', plugins_url( '', __FILE__ ) );
define( 'DM3MEDIA_PATH', plugin_dir_path( __FILE__ ) );
define( 'DM3MEDIA_META_KEY', 'dm3-media' );

class Dm3Media {
	/**
	 * Determine the type of the media, given url.
	 *
	 * @param string $src
	 * @return string | null
	 */
	static function getMediaType( $src ) {
		if ( preg_match( '#^http(s)?://(www\.)?(youtube.com|youtu.be|vimeo.com){1}#i', $src ) ) {
			return 'video';
		}

		$parts = explode( '.', $src );
		$ext = $parts[ count( $parts ) - 1 ];

		switch ( $ext ) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			case 'gif':
			case 'tiff':
				return 'image';
				break;

			case 'mp4':
			case 'm4v':
				return 'video';
				break;

			case 'mp3':	
			case 'ogg':
			case 'm4a':
				return 'audio';
				break;

			default:
				return null;
		}
	}

	/**
	 * Get the media items of a post.
	 *
	 * @param int $postID
	 * @return array
	 */
	static function getPostMedia( $postID ) {
		$media = get_post_meta( $postID, DM3MEDIA_META_KEY, true );

		if ( ! is_array($media ) ) {
			return array();
		}

		return $media;
	}
}

/**
 * Initialize admin.
 */
function dm3media_init() {
	if ( is_admin() ) {
		require_once DM3MEDIA_PATH . '/dm3-media-admin.php';
		$dm3_media_admin = new Dm3MediaAdmin();
	}
}
add_action( 'init', 'dm3media_init' );

/**
 * Load text domain.
 */
function dm3media_textdomain() {
	if ( is_admin() ) {
		load_plugin_textdomain( 'dm3-media', false, 'dm3-media/languages' );
	}
}
add_action( 'plugins_loaded', 'dm3media_textdomain' );
